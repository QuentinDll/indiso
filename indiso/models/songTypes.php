<?php

/**
 * Création de la classe songTypes
 */
class songTypes extends database {

    //Liste des attributs
    public $id = 0;
    public $genre = '';

    public function __construct() {
        parent::__construct();
        $this->dbConnect();
    }

    /**
     * Méthode pour afficher les genres de musique
     * @return type
     */
    public function getSongTypes() {
        $state = false;
        $result = $this->db->query('SELECT `id`, `genre` FROM `UpQ69_songTypes`');
        if ($result->execute()) {
            $selectResult = $result->fetch(PDO::FETCH_OBJ);
            if (is_object($selectResult)) {
                $this->id = $selectResult->id;
                $this->genre = $selectResult->genre;
                $state = true;
            }
        }
        return $state;
    }

    /**
     * Méthode destruct
     */
    public function __destruct() {
        
    }

}
