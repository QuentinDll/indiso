<?php

/**
 * Création de la classe users
 */
class users extends database {

    //Liste des attributs
    public $id = 0;
    public $login = '';
    public $lastname = '';
    public $firstname = '';
    public $email = '';
    public $password = '';

    public function __construct() {
        parent::__construct();
        $this->dbConnect();
    }

    /**
     * Méthode pour vérifié si un utilisateur existe
     * @return type
     */
    public function checkIfUserExist() {
        $state = false;
        $query = 'SELECT COUNT(`id`) AS `count` FROM `UpQ69_users` WHERE `login` = :login';
        $result = $this->db->prepare($query);
        $result->bindValue(':login', $this->login, PDO::PARAM_STR);
        if ($result->execute()) {
            $selectResult = $result->fetch(PDO::FETCH_OBJ);
            $state = $selectResult->count;
        }
        return $state;
    }

    /**
     * Méthode pour inscrire un utilisateur
     * @return type
     */
    public function usersRegister() {
        $query = 'INSERT INTO `UpQ69_users` (`login`, `lastname`, `firstname`, `email`, `password`) '
                . 'VALUES (:login, :lastname, :firstname, :mail, :password)';
        $result = $this->db->prepare($query);
        $result->bindValue(':login', $this->login, PDO::PARAM_STR);
        $result->bindValue(':lastname', $this->lastname, PDO::PARAM_STR);
        $result->bindValue(':firstname', $this->firstname, PDO::PARAM_STR);
        $result->bindValue(':mail', $this->email, PDO::PARAM_STR);
        $result->bindValue(':password', $this->password, PDO::PARAM_STR);
        return $result->execute();
    }

    /**
     * Méthode userConnection connecté un utilisateur 
     * @return type
     */
    public function userConnection() {

        $state = false;
        $query = 'SELECT `id`, `password` FROM `UpQ69_users` WHERE `login` = :login';
        $result = $this->db->prepare($query);
        $result->bindValue(':login', $this->login, PDO::PARAM_STR);
        if ($result->execute()) {
            $selectResult = $result->fetch(PDO::FETCH_OBJ);
            if (is_object($selectResult)) {
                $this->password = $selectResult->password;
                $this->id = $selectResult->id;
                $state = true;
            }
        }
        return $state;
    }

    /**
     * Méthode pour afficher un utilisateur
     * @return type
     */
    public function getUser() {
        $state = false;
        $query = 'SELECT `id`, `login`, `lastname`, `firstname`, `email`, `password` FROM `UpQ69_users` WHERE `id` = :id';
        $result = $this->db->prepare($query);
        $result->bindValue(':id', $this->id, PDO::PARAM_INT);
        if ($result->execute()) {
            $selectResult = $result->fetch(PDO::FETCH_OBJ);
            if (is_object($selectResult)) {
                $this->id = $selectResult->id;
                $this->login = $selectResult->login;
                $this->lastname = $selectResult->lastname;
                $this->firstname = $selectResult->firstname;
                $this->email = $selectResult->email;
                $this->password = $selectResult->password;
                $state = true;
            }
        }
        return $state;
    }

    /**
     * Méthode pour modifier un utilisateur
     * @return type
     */
    public function updateUser() {
        $result = $this->db->prepare('UPDATE `UpQ69_users` SET `login` = :login, `email` = :email WHERE `id` = :id;');
        $result->bindValue(':id', $this->id, PDO::PARAM_INT);
        $result->bindValue(':login', $this->login, PDO::PARAM_STR);
        $result->bindValue(':email', $this->email, PDO::PARAM_STR);
        return $result->execute();
    }

    /**
     * Méthode pour modifier un utilisateur
     * @return type
     */
    public function updateUserPassword() {
        $result = $this->db->prepare('UPDATE `UpQ69_users` SET `password` = :password WHERE `id` = :id;');
        $result->bindValue(':id', $this->id, PDO::PARAM_INT);
        $result->bindValue(':password', $this->password, PDO::PARAM_STR);
        return $result->execute();
    }

    /**
     * Méthode pour supprimer un utilisateur
     * @return type
     */
    public function deleteUser() {
        $remove = $this->db->prepare('DELETE FROM `UpQ69_users` WHERE `id` = :id');
        $remove->bindValue(':id', $this->id, PDO::PARAM_INT);
        return $remove->execute();
    }

    /**
     * Méthode destruct
     */
    public function __destruct() {
        
    }

}
