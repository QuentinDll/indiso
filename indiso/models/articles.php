<?php

/**
 * Création de la classe articles
 */
class articles extends database {

    //Liste des attributs
    public $id = 0;
    public $name = '';
    public $prices = '';
    public $link = '';
    public $producerName = '';
    public $id_UpQ69_songTypes = '';

    public function __construct() {
        parent::__construct();
        $this->dbConnect();
    }

    public function songUpload() {
        $query = 'INSERT INTO `UpQ69_articles` (`name`, `prices`, `link`, `id_UpQ69_songTypes`, `producerName`) VALUES (:name, :prices, :link, :id_UpQ69_songTypes, :producerName)';
        $result = $this->db->prepare($query);
        $result->bindValue(':name', $this->name, PDO::PARAM_STR);
        $result->bindValue(':prices', $this->prices, PDO::PARAM_INT);
        $result->bindValue(':link', $this->link, PDO::PARAM_STR);
        $result->bindValue(':id_UpQ69_songTypes', $this->id_UpQ69_songTypes, PDO::PARAM_INT);
        $result->bindValue(':producerName', $this->producerName, PDO::PARAM_STR);
        return $result->execute();
    }

    /**
     * Méthode pour afficher les musiques
     * @return type
     */
    public function getSong() {
        $result = $this->db->query('SELECT `UpQ69_articles`.`id`, `UpQ69_articles`.`name`, `UpQ69_articles`.`prices`, `UpQ69_articles`.`link`, `UpQ69_articles`.`producerName`, `UpQ69_songTypes`.`genre` '
                . 'FROM `UpQ69_articles` '
                . 'LEFT JOIN `UpQ69_songTypes` '
                . 'ON `UpQ69_songTypes`.`id` = `UpQ69_articles`.`id_UpQ69_songTypes` '
                . 'WHERE `UpQ69_articles`.`id`');
        if (is_object($result)) {
            $result = $result->fetchAll(PDO::FETCH_OBJ);
        }
        return $result;
    }

    /**
     * Méthode pour afficher les musiques par genre
     * @return type
     */
    public function getSongByType() {
        $result = $this->db->prepare('SELECT `UpQ69_articles`.`id`, `UpQ69_articles`.`name`, `UpQ69_articles`.`prices`, `UpQ69_articles`.`link`, `UpQ69_articles`.`producerName`, `UpQ69_songTypes`.`genre` '
                . 'FROM `UpQ69_articles` '
                . 'LEFT JOIN `UpQ69_songTypes` '
                . 'ON `UpQ69_songTypes`.`id` = `UpQ69_articles`.`id_UpQ69_songTypes` '
                . 'WHERE `UpQ69_articles`.`id_UpQ69_songTypes` = :id');
        $result->bindValue(':id', $this->id, PDO::PARAM_INT);
        $result->execute();
        if (is_object($result)) {
            $result = $result->fetchAll(PDO::FETCH_OBJ);
        }
        return $result;
    }

    /**
     * Méthode destruct
     */
    public function __destruct() {
        
    }

}
