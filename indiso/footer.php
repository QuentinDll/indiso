<footer>
    <div class="container text-center text-md-left" id="footer-up">
        <div class="row">
            <div class="col-md-4 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4"><?= FOOTER_LEGAL_TITLE ?></h5>
                <ul class="list-unstyled">
                    <li><a href="#" class="link-footer"><?= FOOTER_LEGAL_CHART ?></a></li>
                    <li><a href="#" class="link-footer"><?= FOOTER_LEGAL_MENTION ?></a></li>
                </ul>
            </div>
            <hr class="clearfix w-100 d-md-none">
            <div class="col-md-4 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4"><?= FOOTER_CONTACT_TITLE ?></h5>
                <ul class="list-unstyled">
                    <!--                    
                    <li><a href="https://www.linkedin.com/in/quentin-delplace-2b75a4166/" id="link-footer">My Linkedin</a></li>
                    <li><a href="https://gitlab.com/QuentinDll" id="link-footer">My GitLab</a></li>
                    <li><a href="https://github.com/QuentinDll" id="link-footer">My GitHub</a></li>
                    <li><a href="https://quentindll.github.io/" id="link-footer">My resume</a></li>
                    -->
                    <li><a href="../views/contact.php" class="link-footer"><?= FOOTER_CONTACT_TEXT ?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container text-center text-md-left" id="footer-social">
        <ul class="list-unstyled list-inline text-center">
            <li class="list-inline-item">
                <a class="btn-floating btn-tw mx-4" href="">
                    <i class="fab fa-twitter fa-2x"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-fb mx-4" href="">
                    <i class="fab fa-facebook-f fa-2x"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-insta mx-4" href="">
                    <i class="fab fa-instagram fa-2x"></i>
                </a> 
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-yt mx-4" href="https://www.youtube.com/channel/UC_OWVkuvjsFq80hwjy0WE8w">
                    <i class="fab fa-youtube fa-2x"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-sc mx-4" href="">
                    <i class="fab fa-soundcloud fa-2x"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="footer-copyright text-center py-3" id="footer-copyright">© 2018 Copyright :
        <a href="#"> IndiSo.com</a>
    </div>
</footer>        
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="../assets/js/script.js"></script>
<script src="../assets/js/audio.js"></script>
</body>
</html>
