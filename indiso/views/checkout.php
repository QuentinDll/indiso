<?php
include_once '../header.php';
include_once '../controllers/checkoutCtrl.php';
?>
<div id="containerCheckout">
    <div class="container">
        <div class="row">
            <h1>Checkout</h1>
        </div>
        <div class="row">
            <div class="col-12" id="userUpdate"></div>
        </div>
    </div>
</div>
<?php include '../footer.php'; ?>