<?php
include_once '../header.php';
include_once '../controllers/userProfilsCtrl.php';
?>
<div id="containeProfils">
    <div class="container-fluid">
        <?php if (isset($_SESSION['isConnect'])) { ?>
            <div class="row">
                <h1>Welcome <?= $user->lastname ?> <?= $user->firstname ?></h1>
            </div>
            <div class="col-12" id="userUpdate">

                <div class="row">
                    <div class="col-6">
                        <form method="post" action="" class="text-center">
                            <div class="form-row">
                                <div class="form-group col-8 has-error">
                                    <label for="login"><?= SIGNUP_LOGIN ?></label>
                                    <input type="text" class="form-control form-control-lg" id="login" name="login" value="<?= $user->login ?>"/>
                                    <?php if (isset($formError['login'])) { ?>
                                        <p class="text-danger"><?= isset($formError['login']) ? $formError['login'] : ''; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-8 has-error">
                                    <label for="email"><?= SIGNUP_MAIL ?></label>
                                    <input type="email" class="form-control form-control-lg" id="email" name="email" value="<?= $user->email ?>"/>
                                    <?php if (isset($formError['email'])) { ?>
                                        <p class="text-danger"><?= isset($formError['email']) ? $formError['email'] : ''; ?></p>
                                    <?php } ?>                    
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-8 has-error">
                                    <?php if (isset($formSucces['update'])) { ?>
                                        <p class="text-danger"><?= isset($formError['update']) ? $formError['update'] : ''; ?></p>
                                    <?php } ?>                                    
                                    <button type="submit" class="btn btn-primary btn-lg col-4" id="btn-form" name="update"><?= USER_PROFIL_UPDATE ?></button>
                                </div>
                            </div>
                            <?php if (isset($formSucces['update'])) { ?>
                                <p class=""><?= isset($formSucces['update']) ? $formSucces['update'] : ''; ?></p>
                            <?php } ?>
                        </form>
                    </div>
                    <div class="col-6">
                        <form method="post" action="" class="text-center">
                            <div class="form-row">
                                <div class="form-group col-8 has-error">
                                    <label for="oldPassword"><?= USER_PROFIL_OLDPASSWORD ?></label>
                                    <input type="password" class="form-control form-control-lg" id="oldPassword" name="oldPassword"/>
                                    <?php if (isset($formError['oldPassword'])) { ?>
                                        <p class="text-danger"><?= isset($formError['oldPassword']) ? $formError['oldPassword'] : ''; ?></p>
                                    <?php } ?>                    
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-8 has-error">
                                    <label for="newPassword"><?= USER_PROFIL_NEWPASSWORD ?></label>
                                    <input type="password" class="form-control form-control-lg" id="newPassword" name="newPassword"/>
                                    <?php if (isset($formError['newPassword'])) { ?>
                                        <p class="text-danger"><?= isset($formError['newPassword']) ? $formError['newPassword'] : ''; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-8 has-error">
                                    <label for="newPasswordVerify"><?= USER_PROFIL_NEWPASSWORD_VERIFY ?></label>
                                    <input type="password" class="form-control form-control-lg" id="newPasswordVerify" name="newPasswordVerify"/>
                                    <?php if (isset($formError['newPasswordVerify'])) { ?>
                                        <p class="text-danger"><?= isset($formError['newPasswordVerify']) ? $formError['newPasswordVerify'] : ''; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-8 has-error">
                                    <button type="submit" class="btn btn-primary btn-lg col-4" id="btn-form" name="passwordUpdate"><?= USER_PROFIL_PASSWORD_UPDATE ?></button>
                                </div>
                            </div>
                            <?php if (isset($formSucces['newPassword'])) { ?>
                                <p class=""><?= isset($formSucces['newPassword']) ? $formSucces['newPassword'] : ''; ?></p>
                            <?php } ?>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10"></div>
                    <form method="post" action="?idRemove=<?= $user->id ?>">
                        <?php if (isset($_POST['delete'])) { ?>
                            <script>
                                alert('<p><?= USER_PROFIL_DELETED ?></p>');
                                window.location = "../index.php";
                            </script>
                        <?php } ?>
                        <button type="submit" class="btn btn-danger btn-lg" id="btn-delete" name="delete"><?= USER_PROFIL_DELETE ?></button>
                    </form>
                </div>
            </div>
        <?php } else { ?>
            <h1><?= SIGNIN_ACCESS_DENIED ?></h1>
            <p><?= SIGNUP_HELP_TEXT ?></p>
        <?php } ?>
    </div>
</div>
<?php include '../footer.php'; ?>
