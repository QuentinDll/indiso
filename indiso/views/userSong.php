<?php
include_once '../header.php';
include_once '../controllers/userSongCtrl.php';
?>
<div class="container">
    <div class="userSongTable">
        <div class="row">
            <h1>Your song</h1>
        </div>
        <div class="row">
            <div class="col-12" id="userUpdate">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Producer Name</th>
                            <th scope="col">Genre</th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($getSong as $song) {
                            ?>
                            <tr>
                                <td><?= $song->name ?></td>
                                <td><?= $song->producerName ?></td>
                                <td><?= $song->genre ?></td>
                                <td><?= $song->prices ?></td>
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include '../footer.php'; ?>
