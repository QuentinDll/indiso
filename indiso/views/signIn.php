<?php
include_once '../header.php';
include_once '../controllers/signInCtrl.php';
?>
<link rel="stylesheet" href="../assets/css/signIn.css"/>

<div class="wrapper fadeInDown">
    <?php if (isset($_POST['connect']) && (count($formError) === 0 )) { ?>
        <script>window.location = "../index.php";</script>
    <?php } else { ?>
        <div id="formContent">
            <div class="fadeIn first">
                <h2><?= SIGNIN_TITLE ?></h2>
            </div>
            <form name="signIn" method="POST" action="#">
                <input type="text" class="form-control form-control-lg fadeIn second" id="login" name="login"/>
                <?php if (isset($formError['login'])) { ?>
                    <p class="text-danger"><?= isset($formError['login']) ? $formError['login'] : ''; ?></p>
                <?php } ?>

                <input type="password" class="form-control form-control-lg fadeIn third" id="password" name="password"/>
                <?php if (isset($formError['password'])) { ?>
                    <p class="text-danger"><?= isset($formError['password']) ? $formError['password'] : ''; ?></p>
                <?php } ?>

                <?php if (isset($formError['match'])) { ?>
                    <p class="text-danger"><?= isset($formError['match']) ? $formError['match'] : ''; ?></p>
                <?php } ?>
                <input type="submit" class="btn btn-secondary btn-lg fadeIn fourth" name="connect"/>
                <?php if (isset($formError['no'])) { ?>
                    <p class="text-danger"><?= isset($formError['no']) ? $formError['no'] : ''; ?></p>
                <?php } ?>
            </form>
            <div id="formFooter">
                <p class="title"><?= SIGNIN_HELP_TITLE ?></p>
                <p> <?= SIGNIN_HELP_TEXT ?></p>
            </div>
        </div>
    </div>
<?php } ?>
<?php include_once '../footer.php'; ?>