<?php
include_once '../header.php';
include_once '../controllers/signInCtrl.php';
?>  
<div class="container" id="containeSignUp">
    <div class="panel-body">
        <?php if (isset($_POST['register']) && (count($formError) === 0 )) { ?>
            <script>window.location = "../index.php";</script>
        <?php } else { ?>
            <div class="row">
                <form class="text-center border border-light col-md-5 div-form" name="addUsers" method="POST" action="#">
                    <h2 class="text-center"><?= SIGNUP_TITLE ?></h2>
                    <div class="form-row">
                        <div class="form-group col-md-12 has-error">
                            <label for="login"><?= SIGNUP_LOGIN ?></label>
                            <input type="text" class="form-control form-control-lg" id="login" name="login"/>
                            <p class="text-danger"><?= isset($formError['login']) ? $formError['login'] : ''; ?></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 has-error">
                            <label for="password"><?= SIGNUP_PASSWORD ?></label>
                            <input type="password" class="form-control form-control-lg" id="password" name="password"/>
                            <p class="text-danger"><?= isset($formError['password']) ? $formError['password'] : ''; ?></p>
                        </div>
                    </div>
                    <p class="text-danger"><?= isset($formError['connect']) ? $formError['connect'] : ''; ?></p>
                    <button type="submit" class="btn btn-primary btn-lg col-md-6" id="btn-form" name="register"><?= SIGNUP_SUBMIT ?></button>
                </form>
                <div class="col-md-2"></div>
                <div class="text-center border border-light col-md-5 divSideForm" id="noSignIn">
                    <p class="title"><?= SIGNIN_HELP_TITLE ?></p>
                    <p> <?= SIGNIN_HELP_TEXT ?></p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php include '../footer.php'; ?>