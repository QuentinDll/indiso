<?php
include_once '../header.php';
?>
<div class="container" id="containerCat">
    <div class="row">
        <div class="content-catergorie col-5 text-center" id="rapHipHop" style="cursor: pointer;" onclick="window.location = 'rapCat.php?id=1';">
            <h2><?= CAT_TITLE_RAP ?></h2>
        </div>
        <div class="col-auto"></div>
        <div class="content-catergorie col-5 text-center" id="popRock" style="cursor: pointer;" onclick="window.location = 'popCat.php?id=2';">
            <h2><?= CAT_TITLE_POP ?></h2>
        </div>
        <div class="col-auto"></div>
        <div class="content-catergorie col-5 text-center" id="hardRockMetal" style="cursor: pointer;" onclick="window.location = 'metalCat.php?id=3';">
            <h2><?= CAT_TITLE_METAL ?></h2>
        </div>
        <div class="col-auto"></div>
        <div class="content-catergorie col-5 text-center" id="ambient" style="cursor: pointer;" onclick="window.location = 'ambientCat.php?id=4';">
            <h2><?= CAT_TITLE_AMBIENT ?></h2>
        </div>
        <div class="col-auto"></div>
        <div class="content-catergorie col-5 text-center" id="soulFunk" style="cursor: pointer;" onclick="window.location = 'funkCat.php?id=5';">
            <h2><?= CAT_TITLE_FUNK ?></h2>
        </div>
        <div class="col-auto"></div>
        <div class="content-catergorie col-5 text-center" id="effect" style="cursor: pointer;" onclick="window.location = 'effectCat.php?id=6';">
            <h2><?= CAT_TITLE_EFFECT ?></h2>
        </div>
    </div>
</div>
<?php include '../footer.php'; ?>