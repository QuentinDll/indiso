<?php
include_once '../header.php';
include_once '../controllers/addSongCtrl.php';
?>
<div class="container" id="containerAddSong">
    <?php if (isset($_SESSION['isConnect'])) { ?>
        <div class="panel-body">
            <div class="row">
                <?php if (isset($_POST['addSong']) && (count($formError) === 0)) { ?>
                    <script> window.location = "../views/userSong.php";</script>
                <?php } else { ?>
                    <div class="col-12">
                        <h2><?= ADDSONG_TITLE ?></h2>
                        <form class="text-center" id="addSongForm" name="addSong" method="post" action="#">
                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="songName"><?= ADDSONG_SONG_NAME ?></label>
                                    <input type="text" class="form-control" id="songName" name="songName" value="<?= isset($songName) ? $songName : '' ?>" placeholder="<?= ADDSONG_SONG_NAME_PLACEHOLDER ?>"/>
                                    <?php if (isset($formError['songName'])) { ?>
                                        <p class="text-danger"><?= isset($formError['songName']) ? $formError['songName'] : ''; ?></p>
                                    <?php } ?>
                                </div>
                                <div class="form-group col-6">
                                    <label for="producerName"><?= ADDSONG_PRODUCER_NAME ?></label>
                                    <input type="text" class="form-control" id="producerName" name="producerName" value="<?= $_SESSION['login']; ?>" placeholder="<?= ADDSONG_PRODUCER_NAME_PLACEHOLDER ?>"/>
                                    <?php if (isset($formError['producerName'])) { ?>
                                        <p class="text-danger"><?= isset($formError['producerName']) ? $formError['producerName'] : ''; ?></p>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="songType"><?= ADDSONG_SONG_TYPE ?></label>
                                    <select id="songType" class="form-control" name="songType">
                                        <option selected disabled><?= ADDSONG_SONG_TYPE_PLACEHOLDER ?></option>
                                        <?php foreach ($genreList as $genre) { ?>
                                            <option value="<?= $genre->id ?>"><?= $genre->genre ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="filesToUp"><?= ADDSONG_FILE ?></label>
                                    <input type="file" class="" id="filesToUp" name="filesToUp"/>
                                    <?php if (isset($formError['filesToUp'])) { ?>
                                        <p class="text-danger"><?= isset($formError['filesToUp']) ? $formError['filesToUp'] : ''; ?></p>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-10"></div>
                                <div class="form-group col-2">
                                    <label for="prices"><?= ADDSONG_PRICES ?></label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="number" class="form-control" id="prices" name="prices" placeholder="0"/>
                                    </div>
                                    <?php if (isset($formError['prices'])) { ?>
                                        <p class="text-danger"><?= isset($formError['prices']) ? $formError['prices'] : ''; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary col-6" name="addSong"><?= ADDSONG_SUBMIT ?></button>
                        </form>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } else { ?>
        <h1><?= SIGNIN_ACCESS_DENIED ?></h1>
        <p><?= SIGNUP_HELP_TEXT ?></p>
    <?php } ?>
</div>
<?php include '../footer.php'; ?>
