<?php
include_once '../header.php';
include_once '../controllers/signUpCtrl.php';
?>
<div class="container" id="containeSignUp">
    <div class="panel-body">
        <div class="row">
            <?php if (isset($_POST['register']) && (count($formError) === 0 )) { ?>
                <script>
                    alert('<?= SIGNUP_SUCCESSFUL ?>');
                    window.location = "../index.php";
                </script>
            <?php } else { ?>
                <form class="text-center border border-light col-md-6 div-form" name="addUsers" method="POST" action="#">
                    <h2 class="text-center"><?= SIGNUP_TITLE ?></h2>
                    <div class="form-row">
                        <div class="form-group col has-error">
                            <label for="login"><?= SIGNUP_LOGIN ?></label>
                            <input type="text" class="form-control form-control-lg" id="login" name="login" placeholder="<?php isset($login) ? $login : '' ?>"/>
                            <p class="loginImport"><?= SIGNUP_LOGIN_IMPORTANT ?></p>
                            <p class="text-danger"><?= isset($formError['login']) ? $formError['login'] : ''; ?></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col has-error">
                            <label for="lastname"><?= SIGNUP_LASTNAME ?></label>
                            <input type="text" class="form-control form-control-lg" id="lastname" name="lastname" placeholder="<?= isset($lastname) ? $lastname : '' ?>"/>
                            <p class="text-danger"><?= isset($formError['lastname']) ? $formError['lastname'] : ''; ?></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col has-error">
                            <label for="firstname"><?= SIGNUP_FIRSTNAME ?></label>
                            <input type="text" class="form-control form-control-lg" id="firstname" name="firstname" placeholder="<?= isset($firstname) ? $firstname : '' ?>"/>
                            <p class="text-danger"><?= isset($formError['firstname']) ? $formError['firstname'] : ''; ?></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col has-error">
                            <label for="mail"><?= SIGNUP_MAIL ?></label>
                            <input type="email" class="form-control form-control-lg" id="mail" name="mail" placeholder="<?= isset($mail) ? $mail : '' ?>"/>
                            <p class="text-danger"><?= isset($formError['mail']) ? $formError['mail'] : ''; ?></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col has-error">
                            <label for="password"><?= SIGNUP_PASSWORD ?></label>
                            <input type="password" class="form-control form-control-lg" id="password" name="password"/>
                            <p class="text-danger"><?= isset($formError['password']) ? $formError['password'] : ''; ?></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col has-error">
                            <label for="passwordVerify"><?= SIGNUP_PASSWORD_VERIFY ?></label>
                            <input type="password" class="form-control form-control-lg" id="passwordVerify" name="passwordVerify"/>
                            <p class="text-danger"><?= isset($formError['passwordVerify']) ? $formError['passwordVerify'] : ''; ?></p>
                        </div>
                    </div>
                    <p class="text-danger"><?= isset($formError['register']) ? $formError['register'] : ''; ?></p>
                    <button type="submit" class="btn btn-primary btn-lg col-md-6" id="btn-form" name="register"><?= SIGNUP_SUBMIT ?></button>
                </form>
            <?php } ?>
            <div class="col-md-1"></div>
            <div class="text-center border border-light col-md-5 divSideForm" id="noSignIn">
                <p class="title"><?= SIGNUP_HELP_TITLE ?></p>
                <p> <?= SIGNUP_HELP_TEXT ?></p>
            </div>
        </div>
    </div>
</div>
<?php include '../footer.php'; ?>
