<?php
include_once '../header.php';
include_once '../controllers/catergorieCtrl.php';
?>
<div class="container" id="containerCat">
    <div class="jumbotron" id="welcomText">
        <h1><?= CAT_TITLE_AMBIENT ?></h1>
        <p><?= CAT_WELCOME_AMBIENT ?></p>
    </div>
</div>
<div class="container container-content">
    <div class="row">
        <nav class="navbar navbar-dark" id="navbar-search">
            <a class="navbar-brand align-items-center" id="navLogo"><?= TOP_MUSIC_TITLE ?></a>
            <div class="form-group has-search align-items-center">
                <form class="navbar-form navbar-right align-items-center" method="post" action="#">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search" id="search-inpt">
                </form>
            </div>
        </nav>
    </div>
    <div class="row">
        <?php include 'defaultPlayer.php'; ?>
    </div>
</div>
<?php include_once '../footer.php'; ?>