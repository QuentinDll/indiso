<div class="content" id="player">
    <div class="d-flex" id="player-top">
        <div class="col mr-auto">
            <h3><?= $song->name ?> - <?= $song->producerName ?></h3>
        </div>
        <div>
            <p><?= $song->prices ?> $</p>
        </div>
    </div>
    <div id="player-center">
        <audio id="audioPlayer" preload="auto" src="assets/audio/<?= $song->link ?>"></audio>
    </div>
    <div id="player-bottom">
        <div class="col mr-auto"></div>
        <a class="" id="addToFavorite" href="#"><i class="fas fa-heart mx-3"></i></a>
        <a class="" id="addTocheckout" href="#" ><i class="fas fa-cart-plus mx-3"></i></a>
    </div>
</div>