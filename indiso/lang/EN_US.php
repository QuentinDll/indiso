<?php

define('TITLE', 'Indiso');

define('PAGE_TITLE_INDEX', 'IndiSo - Home');
define('PAGE_TITLE_SIGNUP', 'IndiSo - Register');
define('PAGE_TITLE_LOGIN', 'IndiSo - Login');
define('PAGE_TITLE_CATEGORY', 'IndiSo - category');
define('PAGE_TITLE_CATEGORY_RAP', 'IndiSo - Rap & Hip-Hop');
define('TOP_MUSIC_TITLE', 'Top Music');

// welcome text
define('WELCOME_TITLE', 'Welcome to IndiSo');
define('WELCOME_TEXT', 'IndiSo is a free platform<br/> where you can purshase copyright free music <br/> made by independant produceur');

// form
define('FORM_LOGIN', 'Please enter your login');
define('FORM_LASTNAME', 'Please enter your lastname');
define('FORM_FIRSTNAME', 'Please enter your firstname');
define('FORM_MAIL', 'Please enter your email address');
define('FORM_PASSWORD', 'Please enter your password');
define('FORM_LOGIN_SUBMIT', 'Login');
define('LOGIN_TITLE', 'Login');
define('USER_CONNECTION_SUCCESS', 'Login succesfull');
define('USER_CONNECTION_ERROR', 'Login error, please try again');

// sign up
define('SIGNUP_TITLE', 'Sign Up');
define('SIGNUP_LOGIN', 'Login :');
define('SIGNUP_LOGIN_IMPORTANT', 'Remember that your user name will be your producer name when adding song');
define('SIGNUP_LASTNAME', 'Lastname :');
define('SIGNUP_FIRSTNAME', 'Firstname :');
define('SIGNUP_MAIL', 'Email :');
define('SIGNUP_PASSWORD', 'Password :');
define('SIGNUP_PASSWORD_VERIFY', 'Password (redo) :');
define('SIGNUP_SUBMIT', 'Sign Up');
define('SIGNUP_SUCCESSFUL', 'Your registration has been taken into account. Welcome to IndiSo');
define('SIGNUP_HELP_TITLE', 'Already have an account?');
define('SIGNUP_HELP_TEXT', 'Click <a href="signIn.php">here</a> to sign in');

// sign in
define('SIGNIN_TITLE', 'Sign In');
define('SIGNIN_SUCCESSFUL', 'Welcome back');
define('SIGNIN_FAIL', 'An error as occured, please retry later');
define('SIGNIN_HELP_TITLE', 'You don\'t have an account?');
define('SIGNIN_HELP_TEXT', 'Don\'t worry click <a href="signUp.php" class="underlineHover">here</a> to create one');
define('SIGNIN_ACCESS_DENIED', 'You need to be connected to see this page');

// user profil
define('USER_PROFIL_OLDPASSWORD', 'Old password :');
define('USER_PROFIL_NEWPASSWORD', 'New password :');
define('USER_PROFIL_NEWPASSWORD_VERIFY', 'New password (redo) :');
define('USER_PROFIL_UPDATE', 'Update');
define('USER_PROFIL_PASSWORD_UPDATE', 'Update Password');
define('USER_PROFIL_DELETE', 'Delete');
define('USER_PROFIL_DELETED', 'Hope to see you again soon');

// add song
define('ADDSONG_TITLE', 'Add your song');
define('ADDSONG_SONG_NAME', 'Song Name');
define('ADDSONG_SONG_NAME_PLACEHOLDER', 'Name your song');
define('ADDSONG_PRODUCER_NAME', 'Producer Name');
define('ADDSONG_PRODUCER_NAME_PLACEHOLDER', 'What your name');
define('ADDSONG_SONG_TYPE', 'Song Type');
define('ADDSONG_SONG_TYPE_PLACEHOLDER', 'Choose a type');
define('ADDSONG_FILE', 'Choose your song (MP3 | max size 50Mo)');
define('ADDSONG_PRICES', 'Choose your price');
define('ADDSONG_SUBMIT', 'Upload');

// Error
define('ERROR_SIGNUP_LOGIN_INVALID', 'Invalid username, please verify');
define('ERROR_SIGNUP_LASTNAME_INVALID', 'Invalid lastname, please verify');
define('ERROR_SIGNUP_FIRSTNAME_INVALID', 'Invalid firstname, please verify');
define('ERROR_SIGNUP_MAIL_INVALID', 'Invalid email, please verify');
define('ERROR_SIGNUP_PASSWORD_INVALID', 'Your password are not the same');
define('ERROR_SIGNUP', 'An error as occured, please retry later');

define('ERROR_SIGNIN', 'Nobody have that name or you\'ve enter the wrong password');
define('ERROR_SIGNIN_COMBINATION', 'This combination doesn\'t exist');
define('ERROR_SIGNIN_PASSWORD', 'Check your password');
define('ERROR_SIGNIN_LOGIN', 'Check your login');

define('ERROR_UPDATE_OLDPASSWORD', 'Wrong password');

define('ERROR_ADDSONG', 'An error as occured, please retry later');
define('ERROR_ADDSONG_NAME_INVALID', 'Invalid name');
define('ERROR_ADDSONG_NAME_EMPTY', 'Please enter a name');
define('ERROR_ADDSONG_SONG_TYPE_INVALID', 'Invalid song type');
define('ERROR_ADDSONG_SONG_TYPE_EMPTY', 'Please select a genre');
define('ERROR_ADDSONG_PRICES_INVALID', 'Invalid price');
define('ERROR_ADDSONG_PRICES_EMPTY', 'Please choose a price');
define('ERROR_ADDSONG_FILE_INVALID', 'Le format d\'image est invalide');
define('ERROR_ADDSONG_FILE_EMPTY', 'Veuillez mettre une image');

// nav
define('NAV_CATEGORY', 'Category');
define('NAV_SIGNIN', 'Sign In');
define('NAV_SIGNOUT', 'Sign Out');
define('NAV_SIGNUP', 'Sign Up');
define('NAV_PROFIL', 'Profil');
define('NAV_USER_SONG', 'Your song');
define('NAV_LANGUAGE', 'Language choice');
define('NAV_FRENCH', 'French');
define('NAV_ENGLISH', 'English');

// category
define('CAT_TITLE_RAP', 'Rap & Hip-Hop');
define('CAT_WELCOME_RAP', 'Here you can find all the Rap and HipHop that you need');

define('CAT_TITLE_POP', 'Pop & Rock');
define('CAT_WELCOME_POP', 'Here you can find all the Pop and Rock that you need');

define('CAT_TITLE_METAL', 'HardRock & Metal');
define('CAT_WELCOME_METAL', 'Here you can find all the HardRock and Metal that you need');

define('CAT_TITLE_AMBIENT', 'Ambient');
define('CAT_WELCOME_AMBIENT', 'Here you can find all the Ambient that you need');

define('CAT_TITLE_FUNK', 'Soul & Funk');
define('CAT_WELCOME_FUNK', 'Here you can find all the Funk and Soul that you need');

define('CAT_TITLE_EFFECT', 'Effect');
define('CAT_WELCOME_EFFECT', 'Here you can find all the Effect that you need');

// footer
define('FOOTER_LEGAL_TITLE', 'Legal links');
define('FOOTER_LEGAL_CHART', 'Privacy charter');
define('FOOTER_LEGAL_MENTION', 'Legal mention');
define('FOOTER_CONTACT_TITLE', 'Contacts');
define('FOOTER_CONTACT_TEXT', 'A problem, Want to contact us click here');
