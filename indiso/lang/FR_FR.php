<?php

define('PAGE_TITLE_INDEX', 'IndiSo - Acceuile');
define('PAGE_TITLE_REGISTER', 'IndiSo - Création de compte');
define('PAGE_TITLE_LOGIN', 'IndiSo - Connexion');
define('PAGE_TITLE_CATERGORI', 'IndiSo - Catergorie');
define('PAGE_TITLE_CATERGORI_RAP', 'IndiSo - Rap & Hip-Hop');

// Formulaire de connexion
define('FORM_LOGIN', 'Veuillez entrer votre identifiant');
define('FORM_PASSWORD', 'Veuillez entrer votre mot de passe');
define('FORM_LOGIN_SUBMIT', 'Se connecter');
define('LOGIN_TITLE', 'Connexion');
define('USER_CONNECTION_SUCCESS', 'Connecté avec succès');
define('USER_CONNECTION_ERROR', 'Erreur lors de la connexion');

// Formulaire d'inscription
define('REGISTER_TITLE', 'Enregistrement d\'un utilisateur');
define('REGISTER_LOGIN', 'Identifiant :');
define('REGISTER_LASTNAME', 'Nom :');
define('REGISTER_FIRSTNAME', 'Prénom :');
define('REGISTER_EMAIL', 'Adresse mail :');
define('REGISTER_PASSWORD', 'Mot de passe :');
define('REGISTER_PASSWORD_VERIFY', 'Mot de passe (vérification) :');
define('REGISTER_SUBMIT', 'Enregistrer');

// Error
define('ERROR_REGISTER_LOGIN_INVALID', 'Identifiant invalide, réessayer');
define('ERROR_REGISTER_LASTNAME_INVALID', 'Nom, réessayer');
define('ERROR_REGISTER_FIRSTNAME_INVALID', 'Prénom, réessayer');
define('ERROR_REGISTER_EMAIL_INVALID', 'Adresse mail, réessayer');
define('ERROR_REGISTER_PASSWORD_INVALID', 'Mot de passe, réessayer');
define('ERROR_REGISTER_PASSWORD_VERIFY_INVALID', 'Les mot de passe ne sont pas identique');

define('NAV_WELCOME', 'Bienvenue %s');
define('NAV_CONNECT', 'Connexion');
define('NAV_DISCONNECT', 'Déconnexion');
define('NAV_LANGUAGE', 'Choix de la langue');
define('NAV_FRENCH', 'Français');
define('NAV_ENGLISH', 'Anglais');
