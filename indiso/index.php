<?php
session_start();
include_once 'configuration.php';
include_once 'controllers/indexCtrl.php';
include_once 'controllers/playerCtrl.php';
?>
<!DOCTYPE html>
<html ng-app="">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <title>IndiSo</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css"/>
        <link rel="stylesheet" href="assets/css/header.css"/>
        <link rel="stylesheet" href="assets/css/style.css"/>
        <link rel="stylesheet" href="assets/css/audioPlayer.css"/>
        <link rel="stylesheet" href="assets/css/footer.css"/>    
    </head>
    <body>
        <nav class="navbar navbar-expand-md fixed-top navbar-dark" id="topNavbar">
            <ul class="nav nav-bar justify-content-start align-items-center">
                <li>
                    <a class="navbar-brand" href="http://indiso/index.php" id="navLogo">
                        <img src="../assets/img/Indiso5.png" width="50" height="50" alt="Logo indiso"/>
                    </a>
                </li>
                <li><a class="navbar-brand" href="http://indiso/index.php" id="navTitle">IndiSo</a></li>
                <li><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button></li>
            </ul>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto align-items-center">
                </ul>
                <ul class="nav navbar-nav justify-content-end align-items-center">
                    <li>
                        <a class="navlink" href="views/catergorie.php"><?= NAV_CATEGORY ?></a>
                    </li>
                    <li class="nav-item dropdown" id="navbarDropdown">
                        <a class = "nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" id="dropdown-title"><?= NAV_LANGUAGE ?></a>  
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#"><span class="flag-icon flag-icon-us"></span> <?= NAV_ENGLISH ?></a>
                            <a class="dropdown-item" href="#"><span class="flag-icon flag-icon-fr"></span> <?= NAV_FRENCH ?></a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="navlink btn btn-dark" id="addSong" href="views/addSong.php"><i class="fas fa-plus"></i></a>
                    </li>
                    <li class="nav-item">
                        <button type="button" class="navlink btn btn-dark" data-toggle="modal" data-target="#checkoutModal" id="checkout"><i class="fas fa-shopping-cart"></i></button>
                    </li>
                    <li class="nav-item">
                        <?php if (!isset($_SESSION['isConnect'])) { ?>
                            <a class="navlink btn btn-dark" id="signIn" href="views/signIn.php">Sign In</a>
                        <?php } else { ?>
                            <div class="dropdown">
                                <button class="navlink btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?= $_SESSION['login']; ?>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="views/userProfils.php?id=<?= $_SESSION['id'] ?>"><?= NAV_PROFIL ?></a>
                                    <a class="dropdown-item" href="views/userSong.php?id=<?= $_SESSION['id'] ?>"><?= NAV_USER_SONG ?></a>
                                    <a class="dropdown-item" href="<?= $_SERVER['PHP_SELF'] ?>?action=disconnect"><?= NAV_SIGNOUT ?></a>
                                </div>
                            </div>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <div class="jumbotron" id="welcomText">
                <h1><?= WELCOME_TITLE ?></h1>
                <p><?= WELCOME_TEXT ?></p>
            </div>
        </div>
        <div class="container container-content">
            <div class="row">
                <nav class="navbar navbar-dark" id="navbar-search">
                    <a class="navbar-brand align-items-center" id="navSearchTitle"><?= TOP_MUSIC_TITLE ?></a>
                    <div class="form-group has-search align-items-center">
                        <form class="navbar-form navbar-right align-items-center" method="post" action="#">
                            <input class="form-control" type="search" placeholder="Search" aria-label="Search" id="search-inpt">
                        </form>
                    </div>
                </nav>
            </div>
            <div class="row">
                <?php include 'views/defaultPlayer.php'; ?>
            </div>
        </div>
        <footer>
            <div class="container text-center text-md-left" id="footer-up">
                <div class="row">
                    <div class="col-md-4 mx-auto">
                        <h5 class="font-weight-bold text-uppercase mt-3 mb-4"><?= FOOTER_LEGAL_TITLE ?></h5>
                        <ul class="list-unstyled">
                            <li><a href="#" class="link-footer"><?= FOOTER_LEGAL_CHART ?></a></li>
                            <li><a href="#" class="link-footer"><?= FOOTER_LEGAL_MENTION ?></a></li>
                        </ul>
                    </div>
                    <hr class="clearfix w-100 d-md-none">
                    <div class="col-md-4 mx-auto">
                        <h5 class="font-weight-bold text-uppercase mt-3 mb-4"><?= FOOTER_CONTACT_TITLE ?></h5>
                        <ul class="list-unstyled">
                            <!--                    
                            <li><a href="https://www.linkedin.com/in/quentin-delplace-2b75a4166/" id="link-footer">My Linkedin</a></li>
                            <li><a href="https://gitlab.com/QuentinDll" id="link-footer">My GitLab</a></li>
                            <li><a href="https://github.com/QuentinDll" id="link-footer">My GitHub</a></li>
                            <li><a href="https://quentindll.github.io/" id="link-footer">My resume</a></li>
                            -->
                            <li><a href="../views/contact.php" class="link-footer"><?= FOOTER_CONTACT_TEXT ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container text-center text-md-left" id="footer-social">
                <ul class="list-unstyled list-inline text-center">
                    <li class="list-inline-item">
                        <a class="btn-floating btn-tw mx-4" href="">
                            <i class="fab fa-twitter fa-2x"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-fb mx-4" href="">
                            <i class="fab fa-facebook-f fa-2x"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-insta mx-4" href="">
                            <i class="fab fa-instagram fa-2x"></i>
                        </a> 
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-yt mx-4" href="https://www.youtube.com/channel/UC_OWVkuvjsFq80hwjy0WE8w">
                            <i class="fab fa-youtube fa-2x"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-sc mx-4" href="">
                            <i class="fab fa-soundcloud fa-2x"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-copyright text-center py-3" id="footer-copyright">© 2018 Copyright :
                <a href="#"> IndiSo.com</a>
            </div>
        </footer>        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="assets/js/script.js"></script>
        <script src="assets/js/audio.js"></script>
    </body>
</html>
