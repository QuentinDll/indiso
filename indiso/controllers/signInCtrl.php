<?php

$login = '';
$formError = array();
$message = '';

if (isset($_POST['connect'])) {

    if (!empty($_POST['login'])) {
        $login = htmlspecialchars($_POST['login']);
    } else {
        $formError['login'] = ERROR_SIGNIN_LOGIN;
    }

    if (!empty($_POST['password'])) {
        $password = $_POST['password'];
    } else {
        $formError['password'] = ERROR_SIGNIN_PASSWORD;
    }
    if (count($formError) == 0) {
        $user = new users();
        $user->login = $login;
        if ($user->userConnection()) {
            if (password_verify($password, $user->password)) {
                $message = SIGNIN_SUCCESSFUL;
                $_SESSION['id'] = $user->id;
                $_SESSION['login'] = $user->login;
                $_SESSION['lastname'] = $user->lastname;
                $_SESSION['firstname'] = $user->firstname;
                $_SESSION['mail'] = $user->email;
                $_SESSION['isConnect'] = true;
            } else {
                $formError['match'] = ERROR_SIGNIN_COMBINATION;
            }
        } else {
            $formError['no'] = SIGNIN_FAIL;
        }
    }
}
