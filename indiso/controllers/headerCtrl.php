<?php

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'disconnect') {
        session_destroy();
        header('location:http://indiso/views/signIn.php');
    }
}

//choix de la langue
if (!empty($_GET['lang'])) {
    $_SESSION['lang'] = $_GET['lang'];
}
//Si dans la session on a une langue on la charge sinon c'est l'anglais par défaut
include_once '../lang/' . (isset($_SESSION['lang']) ? $_SESSION['lang'] : 'EN_US') . '.php';
