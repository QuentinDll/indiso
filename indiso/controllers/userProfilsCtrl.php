<?php

if (!empty($_GET['id'])) {
    $user = new users();
    $user->id = $_GET['id'];
    $getUser = $user->getUser();
}

$login = '';
$email = '';
$password = '';
$passwordVerify = $password;
$formError = array();
$formSucces = array();

if (isset($_POST['update'])) {
    $user = new users();
    $user->id = $_GET['id'];

    /* verification login */
    if (!empty($_POST['login'])) {
        if ($_POST['login']) {
            $user->login = htmlspecialchars($_POST['login']);
        } else {
            $formError['login'] = ERROR_SIGNUP_LOGIN_INVALID;
        }
    } else {
        $formError['login'] = FORM_LOGIN;
    }
    /* verification email */
    if (!empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $user->email = htmlspecialchars($_POST['email']);
    } else {
        $formError['email'] = ERROR_SIGNUP_MAIL_INVALID;
    }

    if (count($formError) == 0) {
        if (!$user->updateUser()) {
            $formError['update'] = ERROR_SIGNUP;
        }
        $user->login = $login;
        $user->email = $email;
    }
}

if (isset($_POST['passwordUpdate'])) {
    $user = new users();
    $user->id = $_GET['id'];
    $hash = $user->password;

    /* verification old password */
    if (!empty($_POST['oldPassword']) && password_verify($_POST['oldPassword'], $hash)) {
        /* verification new password */
        if (!empty($_POST['newPassword']) && !empty($_POST['newPasswordVerify']) && $_POST['newPassword'] == $_POST['newPasswordVerify']) {
            $user->password = password_hash($_POST['newPassword'], PASSWORD_DEFAULT);
            $formSucces['newPassword'] = 'Password changed';
        } else {
            $formError['newPassword'] = ERROR_SIGNUP_PASSWORD_INVALID;
        }
    } else {
        $formError['oldPassword'] = ERROR_UPDATE_OLDPASSWORD;
    }
    if (count($formError) == 0) {
        if (!$user->updateUserPassword()) {
            $formError['update'] = ERROR_SIGNUP;
        }
        $user->password = $password;
    }
}

if (isset($_POST['delete'])) {
    $user = new users();
    if (isset($_GET['idRemove'])) {
        $user->id = $_GET['idRemove'];
        $removeUser = $user->deleteUser();
        if ($user->deleteUser()) {
            session_unset();
            session_destroy();
        }
    }
}