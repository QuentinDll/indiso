<?php

$regexName = '/^[A-Za-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ° \'\-]+$/';
$login = '';
$lastname = '';
$firstname = '';
$mail = '';
$password = '';
$passwordVerify = $password;
$formError = array();

if (isset($_POST['register'])) {
    $user = new users();
    /* verification login */
    if (!empty($_POST['login'])) {
        if ($_POST['login']) {
            $user->login = htmlspecialchars($_POST['login']);
        } else {
            $formError['login'] = ERROR_SIGNUP_LOGIN_INVALID;
        }
    } else {
        $formError['login'] = FORM_LOGIN;
    }

    /* verification lastname */
    if (!empty($_POST['lastname'])) {
        if (preg_match($regexName, $_POST['lastname'])) {
            $user->lastname = htmlspecialchars($_POST['lastname']);
        } else {
            $formError['lastname'] = ERROR_SIGNUP_LASTNAME_INVALID;
        }
    } else {
        $formError['lastname'] = FORM_LASTNAME;
    }

    /* verification firstname */
    if (!empty($_POST['firstname'])) {
        if (preg_match($regexName, $_POST['firstname'])) {
            $user->firstname = htmlspecialchars($_POST['firstname']);
        } else {
            $formError['firstname'] = ERROR_SIGNUP_FIRSTNAME_INVALID;
        }
    } else {
        $formError['firstname'] = FORM_FIRSTNAME;
    }

    /* verification email */
    if (!empty($_POST['mail']) && filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
        $user->email = htmlspecialchars($_POST['mail']);
    } else {
        $formError['mail'] = ERROR_SIGNUP_MAIL_INVALID;
    }

    /* verification password */
    if (!empty($_POST['password']) && !empty($_POST['passwordVerify']) && $_POST['password'] == $_POST['passwordVerify']) {
        $user->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    } else {
        $formError['password'] = ERROR_SIGNUP_PASSWORD_INVALID;
    }

    if (count($formError) == 0) {
        if (!$user->usersRegister()) {
            $formError['register'] = ERROR_SIGNUP;
        }
        $user->login = $login;
        $user->lastname = $lastname;
        $user->firstname = $firstname;
        $user->email = $mail;
        $user->password = $password;
    }
}
