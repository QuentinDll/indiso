<?php

$songTypes = new songTypes();
$genreList = $songTypes->getSongTypes();

$regexprices = '\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2})';
$name = '';
$producerName = '';
$id_UpQ69_songTypes = '';
$prices = '';
$link = '';
$formError = array();

if (isset($_POST['addSong'])) {
    $upload = new articles();

    // verif input song name
    if (!empty($_POST['songName'])) {
        if ($_POST['songName']) {
            $upload->name = htmlspecialchars($_POST['songName']);
        } else {
            $formError['songName'] = ERROR_ADDSONG_NAME_INVALID;
        }
    } else {
        $formError['songName'] = ERROR_ADDSONG_NAME_EMPTY;
    }
    // verif input producer name
    if (!empty($_POST['producerName'])) {
        if ($_POST['producerName']) {
            $upload->producerName = htmlspecialchars($_POST['producerName']);
        } else {
            $formError['producerName'] = 'Invalid name';
        }
    } else {
        $formError['producerName'] = 'please enter a name';
    }

    if (!empty($_POST['songType'])) {
        if ($_POST['songType']) {
            $upload->id_UpQ69_songTypes = htmlspecialchars($_POST['songType']);
        } else {
            $formError['sontType'] = ERROR_ADDSONG_SONG_TYPE_INVALID;
        }
    } else {
        $formError['sontType'] = ERROR_ADDSONG_SONG_TYPE_EMPTY;
    }
    // verif input prices
    if (!empty($_POST['prices'])) {
        if ($_POST['prices']) {
            $upload->prices = htmlspecialchars($_POST['prices']);
        } else {
            $formError['prices'] = ERROR_ADDSONG_PRICES_INVALID;
        }
    } else {
        $formError['prices'] = ERROR_ADDSONG_PRICES_EMPTY;
    }
    // verif input fileToUp
    if (!empty($_POST['filesToUp'])) {
        if ($_POST['filesToUp']) {
            $upload->link = htmlspecialchars($_POST['filesToUp']);
        } else {
            $formError['filesToUp'] = ERROR_ADDSONG_FILE_INVALID;
        }
    } else {
        $formError['filesToUp'] = ERROR_ADDSONG_FILE_EMPTY;
    }

    if (count($formError) == 0) {
        if (!$upload->songUpload()) {
            $formError['upload'] = ERROR_SIGNUP;
        }
        $upload->name = $name;
        $upload->producerName = $producerName;
        $upload->id_UpQ69_songTypes = $id_UpQ69_songTypes;
        $upload->prices = $prices;
        $upload->link = $link;
    }
}
