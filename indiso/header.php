<?php
session_start();
include_once 'configuration.php';
include 'controllers/headerCtrl.php';
?>
<!DOCTYPE html>
<html ng-app="">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <title>IndiSo</title>
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" id="bootstrap-css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/css/header.css"/>
        <link rel="stylesheet" href="../assets/css/style.css"/>
        <link rel="stylesheet" href="../assets/css/audioPlayer.css"/>
        <link rel="stylesheet" href="../assets/css/footer.css"/>
    </head>
    <body>

        <nav class="navbar navbar-expand-md fixed-top navbar-dark" id="topNavbar">
            <ul class="nav nav-bar justify-content-start align-items-center">
                <li>
                    <a class="navbar-brand" href="http://indiso/index.php" id="navLogo">
                        <img src="../assets/img/Indiso5.png" width="50" height="50" alt="Logo indiso"/>
                    </a>
                </li>
                <li><a class="navbar-brand" href="http://indiso/index.php" id="navTitle">IndiSo</a></li>
                <li><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button></li>
            </ul>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto align-items-center">
                </ul>
                <ul class="nav navbar-nav justify-content-end align-items-center">
                    <li>
                        <a class="navlink" href="catergorie.php"><?= NAV_CATEGORY ?></a>
                    </li>
                    <li class="nav-item dropdown" id="navbarDropdown">
                        <a class = "nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" id="dropdown-title"><?= NAV_LANGUAGE ?></a>  
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?= $_SERVER['HTTP_REFERER'] ?>/english/"><span class="flag-icon flag-icon-us"></span> <?= NAV_ENGLISH ?></a>
                            <a class="dropdown-item" href="<?= $_SERVER['HTTP_REFERER'] ?>/francais/"><span class="flag-icon flag-icon-fr"></span> <?= NAV_FRENCH ?></a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="navlink btn btn-dark" id="addSong" href="addSong.php"><i class="fas fa-plus"></i></a>
                    </li>
                    <li class="nav-item">
                        <button type="button" class="navlink btn btn-dark" data-toggle="modal" data-target="#checkoutModal" id="checkout"><i class="fas fa-shopping-cart"></i></button>
                    </li>
                    <li class="nav-item">
                        <?php if (!isset($_SESSION['isConnect'])) { ?>
                            <a class="navlink btn btn-dark" id="signIn" href="signIn.php">Sign In</a>
                        <?php } else { ?>
                            <div class="dropdown">
                                <button class="navlink btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <?= $_SESSION['login']; ?>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="userProfils.php?id=<?= $_SESSION['id'] ?>"><?= NAV_PROFIL ?></a>
                                    <a class="dropdown-item" href="userSong.php?id=<?= $_SESSION['id'] ?>"><?= NAV_USER_SONG ?></a>
                                    <a class="dropdown-item" href="<?= $_SERVER['PHP_SELF'] ?>?action=disconnect"><?= NAV_SIGNOUT ?></a>
                                </div>
                            </div>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </nav>
        -->